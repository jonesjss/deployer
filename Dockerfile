FROM openjdk:8-jdk-slim

RUN apt-get update && apt-get install -y make

ARG NEXUS_MIRROR_URL
ARG NEXUS_USER
ARG NEXUS_PASSWORD

ENV NEXUS_MIRROR_URL  ${NEXUS_MIRROR_URL}
ENV NEXUS_USER ${NEXUS_USER}
ENV NEXUS_PASSWORD ${NEXUS_PASSWORD}
ENV MAVEN_REPO_PREFIX="https://repo.maven.apache.org/maven2/"
ENV MVNW_USERNAME=${NEXUS_USER}
ENV MVNW_PASSWORD=${NEXUS_PASSWORD}

COPY extensions/ /extensions
COPY Makefile.deployer /Makefile

ONBUILD RUN echo "building dist" && make build/dist
ONBUILD WORKDIR /application
ONBUILD COPY ./.mvn/ /application/.mvn
ONBUILD RUN  sed -i "s@${MAVEN_REPO_PREFIX}@${NEXUS_MIRROR_URL}@g" ./.mvn/wrapper/maven-wrapper.properties
ONBUILD COPY ./mvnw /application
ONBUILD RUN  ./mvnw -v
ONBUILD COPY ./pom.xml /application/
ONBUILD RUN  echo "Downloading dependencies" && ./mvnw dependency:resolve --quiet --no-transfer-progress
ONBUILD COPY ./src/ /application/src/
ONBUILD ARG  SKIP_TEST='true'
ONBUILD RUN  echo "Packaging application" && ./mvnw clean package --quiet --no-transfer-progress -DskipTests=${SKIP_TEST}
ONBUILD ARG  BUILD_DIR=/application/target
